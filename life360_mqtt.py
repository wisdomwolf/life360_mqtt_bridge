#!python3

import logging
import time
import requests
import jmespath
import threading
import json
from logging.config import fileConfig
from configparser import ConfigParser
from paho.mqtt import client as paho
from datetime import datetime, date

fileConfig('config/logging_config.ini')
logger = logging.getLogger('Life360_MQTT')
#logger.debug('testing %s', '...1...2...3')

class _LifeBase(object):
    from datetime import date

    def __init__(self, href=None, type=None, uri=None):
        self.href = href
        self.type = type
        self.uri = uri

    def __repr__(self):
        repr_list = []
        for k, v in self.__dict__.items():
            if v:
                if isinstance(v, date):
                    v = '{:%m/%d/%Y}'.format(v)
                k = k.replace('_', ' ')
                repr_list.append('{}={}'.format(k, v))
        return '{}({})'.format(self.__class__.__name__, ', '.join(sorted(repr_list, key=self._sort)))

    def __str__(self):
        str_list = []
        for k, v in self.__dict__.items():
            if v:
                if isinstance(v, date):
                    v = '{:%m/%d/%Y}'.format(v)
                k = k.replace('_', ' ')
                str_list.append('{}: {}'.format(k, v))
        return '\n'.join(sorted(str_list, key=self._sort))

    @staticmethod
    def _sort(key):
        '''Used to ensure certain attributes are listed first'''
        key = key.split(':')[0].lower()
        sort_keys = ['id', 'name'] # Should probably be a class variable so that it can be easily overridden
        try:
            return sort_keys.index(key)
        except ValueError:
            return float('inf')


class LifeMQTTBridge(_LifeBase):

    def __init__(self, config_file=None, username=None, password=None):
        if config_file:
            config = ConfigParser()
            config.read(config_file)
        username = username or config.get('Life 360', 'Username')
        password = password or config.get('Life 360', 'Password')
        mqtt_host = config.get('MQTT', 'Host')
        mqtt_port = int(config.get('MQTT', 'Port')) or 1883
        mqtt_username = config.get('MQTT', 'Username')
        mqtt_password = config.get('MQTT', 'Password')

        self.auth_token = self._get_bearer(username, password)
        self.circle_ids = self._get_circle_ids()
        self.members = []
        for circle_id in self.circle_ids:
            for member in self._get_members(circle_id):
                self.members.append(Life360Member(circle_id=circle_id, auth_token=self.auth_token, **member))

        self.mqtt_client = self._setup_mqtt(mqtt_host, mqtt_port, mqtt_username, mqtt_password)
        self.mqtt_client.loop_start()

    def _setup_mqtt(self, host, port, username=None, password=None):
        client = paho.Client()
        client.on_publish = on_publish
        client.on_connect = on_connect
        if username or password:
            client.username_pw_set(username, password)
            if port == 8883:
                client.tls_set()
        client.connect(host, port)
        return client

    def _get_bearer(self, username, password):
        logger.info(f'{int(time.time())} - requesting access token')
        headers = {'Authorization': 'Basic cFJFcXVnYWJSZXRyZTRFc3RldGhlcnVmcmVQdW1hbUV4dWNyRUh1YzptM2ZydXBSZXRSZXN3ZXJFQ2hBUHJFOTZxYWtFZHI0Vg=='}
        payload = {'grant_type': 'password', 'username': username, 'password': password}
        url = 'https://api.life360.com/v3/oauth2/token.json'
        return requests.post(url, headers=headers, data=payload).json().get('access_token')

    def _get_circle_ids(self):
        logger.info('requesting circles')
        headers = {'Authorization': 'Bearer {}'.format(self.auth_token)}
        url = 'https://api.life360.com/v3/circles.json'
        try:
            circle_json = requests.get(url, headers=headers).json()
            return jmespath.search('circles[*].id', circle_json)
        except json.decoder.JSONDecodeError:
            return None

    def _get_members(self, circle_id):
        logger.info('requesting members')
        headers = {'Authorization': 'Bearer {}'.format(self.auth_token)}
        url = 'https://api.life360.com/v3/circles/{}'.format(circle_id)
        try:
            return requests.get(url, headers=headers).json().get('members')
        except json.decoder.JSONDecodeError:
            return {}


class Life360Member(_LifeBase):

    member_url_base = 'https://api.life360.com/v3/circles/{}/members/{}'

    def __init__(
                    self, id, firstName, lastName, location, 
                    loginEmail=None, 
                    loginPhone=None, 
                    avatar=None, 
                    isAdmin=False, 
                    pinNumber=0, 
                    features=None, 
                    issues=None, 
                    communications=None,
                    medical=None, 
                    relation=None, 
                    createdAt=None, 
                    activity=None,
                    circle_id=None,
                    auth_token=None
                ):
        self.id = id
        self.first_name = firstName
        self.last_name = lastName
        self.email = loginEmail
        self.phone = loginPhone
        self.avatar = avatar
        self.is_admin = bool(int(isAdmin))
        self.pin = pinNumber
        self.relation = relation
        self.creation_date = datetime.fromtimestamp(int(createdAt)).strftime('%c')
        self._update_url = self.member_url_base.format(circle_id, self.id) if circle_id else None
        self._auth_token = auth_token
        self.update()
        
    def update(self):
        update_kwargs = requests.get(self._update_url, headers={'Authorization': 'Bearer {}'.format(self._auth_token)}).json()
        self._update(**update_kwargs)

    def _update(self, *args, **kwargs):
        # Because I'm too lazy to type out all the params individually
        for param, value in kwargs.items():
            setattr(self, param, value)
        if self.location:
            self.latitude = self.location.get('latitude')
            self.longitude = self.location.get('longitude')
            self.location_name = self.location.get('name', 'Unknown')
            self.moving = bool(int(self.location.get('inTransit')))
            self.battery = self.location.get('battery')
            self.charging = bool(int(self.location.get('charge')))
            self.wifi_enabled = bool(int(self.location.get('wifiState')))
            self.last_updated = datetime.fromtimestamp(int(self.location.get('timestamp'))).strftime('%c')
        print('Last updated: {}'.format(self.last_updated))


def on_connect(client, userdata, flags, rc):
	logger.info('CONNACK received with code {}'.format(rc))


def on_publish(client, userdata, mid):
	logger.info('mqtt published successfully. mid: {}'.format(mid))


def get_circles(auth_token):
    logger.info('requesting circles')
    headers = {'Authorization': 'Bearer {}'.format(auth_token)}
    url = 'https://api.life360.com/v3/circles.json'
    try:
        circle_json = requests.get(url, headers=headers).json()
        return jmespath.search('circles[*].id', circle_json)
    except json.decoder.JSONDecodeError:
        return None


def get_members(auth_token, circle_id):
    logger.info('requesting members')
    headers = {'Authorization': 'Bearer {}'.format(auth_token)}
    url = 'https://api.life360.com/v3/circles/{}'.format(circle_id)
    try:
        return requests.get(url, headers=headers).json()
    except json.decoder.JSONDecodeError:
        return {}


def main(bridge):
    circles = get_circles(bridge.auth_token)
    if not circles:
        logger.warn('no circle data')
        threading.Timer(30, main, (bridge,)).start()
        return
    for circle_id in circles:
        members = get_members(bridge.auth_token, circle_id)
        if not members:
            logger.info('members empty, waiting 30 secs')
            threading.Timer(30, main, (bridge,)).start()
            return
        for member in members.get('members'):
            first_name = member.get('firstName')
            last_name = member.get('lastName')
            member_id = member.get('id')
            location = member.get('location')
            latitude = location.get('latitude')
            longitude = location.get('longitude')
            accuracy = location.get('accuracy')
            battery = location.get('battery')
            location_name = location.get('name')
            mqtt_topic = f'owntracks/{first_name}/{member_id}'
            mqtt_message = f'{{"location_name":"{location_name}","tst":{int(time.time())},"acc":{accuracy},"_type":"location","alt":0,"lon":{longitude},"lat":{latitude},"batt":{battery}}}'
            # post to MQTT
            #logger.info(f'mosquitto publish -t {mqtt_topic} -m {mqtt_message}')
            bridge.mqtt_client.publish(mqtt_topic, mqtt_message, qos=2)
    threading.Timer(30, main, (bridge,)).start()

if __name__ == "__main__":
    bridge = LifeMQTTBridge(config_file='config/settings.ini')
    main(bridge)

