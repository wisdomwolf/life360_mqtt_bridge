 ]; then
bearer
circles $bearer_id
members $bearer_id $i
fi

members_id=$(echo $members | jq '.members[].id')
IFS=$'\n' read -rd '' -a members_array <<<"$members_id"
count=0
for i in "${members_array[@]}"
do
    firstName=$(echo $members | jq .members[$count].firstName)
    lastName=$(echo $members | jq .members[$count].lastName)
    latitude=$(echo $members | jq .members[$count].location.latitude)
    longitude=$(echo $members | jq .members[$count].location.longitude)
    accuracy=$(echo $members | jq .members[$count].location.accuracy)
    battery=$(echo $members | jq .members[$count].location.battery)
    locationname=$(echo $members | jq .members[$count].location.name)
    if [ "$locationname" = "\"Home"\" ]; then 
    locationname="ON"
    else
    locationname="OFF"
    fi
    $mosquitto_pub -h $mqtt_host -p $mqtt_port -u $mqtt_user -P $mqtt_pass -t "owntracks/${firstName//\"/}" -m "{\"t\":\"p\",\"tst\":$(date +%s),\"acc\":${accuracy//\"/},\"_type\":\"location\",\"alt\":0,\"lon\":${longitude//\"/},\"lat\":${latitude//\"/},\"batt\": ${battery//\"/},\"locname\": ${locationname//\"/}}"
	
done
done
