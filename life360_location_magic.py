#!/usr/bin/env python3.6

from datetime import datetime as dtime

import googlemaps
from pymongo import MongoClient
from configparser import ConfigParser

config = ConfigParser()
config.read('settings.ini')

mongo_username = config.get('Location Magic', 'mongo_username')
mongo_password = config.get('Location Magic', 'mongo_password')
mongo_host = config.get('Location Magic', 'mongo_host')
mongo_port = config.get('Location Magic', 'mongo_port', fallback='27017')
mongo_collection = config.get('Location Magic', 'mongo_collection')
mongo_auth = config.get('Location Magic', 'mongo_auth_db', fallback='admin')
GOOGLE_API_KEY = config.get('Location Magic', 'google_api')

client = MongoClient(f'mongodb://{mongo_username}:{mongo_password}@{mongo_host}:{mongo_port}/{mongo_collection}?authSource={mongo_auth}')
mongo_db = client.get_database(mongo_collection)
gmaps = googlemaps.Client(key=GOOGLE_API_KEY)


def get_last_location(person):
    location_info = get_last_location_raw(person)
    return f"{person} @ {location_info.get('location_name')}\ncoords: {location_info.get('lat')}, {location_info.get('lon')}\ntime: {dtime.fromtimestamp(location_info.get('tst', 0)).strftime('%c')}"


def get_last_location_raw(person):
    for collection_name in mongo_db.collection_names():
        if person.lower() in collection_name.lower():
            collection = collection_name
            break
    return list(mongo_db[collection].find().sort([('$natural', -1)]).limit(1))[0]


def get_address(person):
    location_info = get_last_location_raw(person)
    return gmaps.reverse_geocode((location_info['lat'], location_info['lon']))


def get_eta(origin_person, destination_person):
    return get_trip_info(origin_person, destination_person, 'duration_in_traffic')


def get_coords(person):
    info = get_last_location_raw(person)
    return (info['lat'], info['lon'])


def get_trip_info(origin_person, destination_person, metric=None):
    directions = get_directions(origin_person, destination_person, departure_time=dtime.now())
    if directions:
        return directions[0].get('legs')[0].get(metric).get('text')
    else:
        return None


def get_directions(origin_person, destination_person, *args, **kwargs):
    origin_location = get_coords(origin_person)
    destination_location = get_coords(destination_person)
    return gmaps.directions(origin_location, destination_location, *args, **kwargs)


def how_far_away(origin_person, destination_person):
    return get_trip_info(origin_person, destination_person, 'distance')
